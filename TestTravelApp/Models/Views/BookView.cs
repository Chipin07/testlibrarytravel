﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTravelApp.Models.Views
{
    public class BookView : Book
    {
        [Display(Name = "Author")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public int AuthorId { get; set; }

        public int AuthorHasBookId { get; set; }
    }
}
