﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestTravelApp.Models
{
    public class AuthorHasBook
    {
        [Key]
        public int AuthorHasBookId { get; set; }

        [ForeignKey("Author")]
        public int AuthorId { get; set; }

        [ForeignKey("Book")]
        public int BookIsbn { get; set; }

        public Book Book { get; set; }
        public Author Author { get; set; }

    }
}
