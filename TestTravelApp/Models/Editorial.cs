﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTravelApp.Models
{
    public class Editorial
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(45, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 2)]
        public string Name { get; set; }

        [Display(Name = "Sede")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(45, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 2)]
        public string Campus { get; set; }

        public List<Book> Books { get; set; }

    }
}
