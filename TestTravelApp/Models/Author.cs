﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestTravelApp.Models
{
    public class Author
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(45, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 2)]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(45, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 2)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName { get { return string.Format("{0} {1}", Name, LastName); } }


        public List<AuthorHasBook> AuthorHasBooks { get; set; }

    }
}
