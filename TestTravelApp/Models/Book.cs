﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestTravelApp.Models
{
    public class Book
    {
        [Key]
        [ForeignKey("Isbn")]
        public int Isbn { get; set; }


        [ForeignKey("Editorial")]
        [Display(Name = "Editorial")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public int EditorialId { get; set; }

        [Display(Name = "Titulo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(45, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 2)]
        public string Title { get; set; }

        [Display(Name = "Sinopsis")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Sinopsis { get; set; }

        [Display(Name = "Numero de paginas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(45, ErrorMessage = "El campo {0} puede tener maximo  {1} caracteres")]
        [RegularExpression("(^[0-9]+$)", ErrorMessage = "Solo se permiten números")]
        public string PageNumbers { get; set; }

        public List<AuthorHasBook> AuthorHasBooks { get; set; }

        public Editorial Editorial { get; set; }


    }
}
