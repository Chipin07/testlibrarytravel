﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestTravelApp.Data;
using TestTravelApp.Models;
using TestTravelApp.Models.Views;

namespace TestTravelApp.Controllers
{
    public class BooksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BooksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Books
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Book.Include(b => b.Editorial);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Books/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.Editorial)
                .FirstOrDefaultAsync(m => m.Isbn == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // GET: Books/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["EditorialId"] = new SelectList(_context.Editorials, "Id", "Name");
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "FullName");
            
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BookView book)
        {
            if (ModelState.IsValid)
            {
               

                using (var transaction = _context.Database.BeginTransaction())
                {

                    try
                    {
                        _context.Add(book);
                        await _context.SaveChangesAsync();

                        var authorHasBook = new AuthorHasBook
                        {
                            AuthorId = book.AuthorId,
                            BookIsbn = book.Isbn,
                        };
                        _context.Add(authorHasBook);
                        _context.SaveChanges();
                        transaction.Commit();
                        TempData["message"] = "Registro creado correctamente";
                        return RedirectToAction(nameof(Index));
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        TempData["message"] = "Error:" + ex.Message;
                    }
                }
            }
            ViewData["EditorialId"] = new SelectList(_context.Editorials, "Id", "Name", book.EditorialId);
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "FullName", book.AuthorId);
            return View(book);
        }

        // GET: Books/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Book.Include("AuthorHasBooks").Where(a => a.Isbn == id).FirstAsync();
            if (book == null)
            {
                return NotFound();
            }
            var authorHasBooks = book.AuthorHasBooks.FirstOrDefault();
            ViewData["EditorialId"] = new SelectList(_context.Editorials, "Id", "Campus", book.EditorialId);
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "FullName", authorHasBooks.AuthorId);

            var bookView = new BookView()
            {
                AuthorId = authorHasBooks.AuthorId,
                Editorial = book.Editorial,
                EditorialId = book.EditorialId,
                Isbn = book.Isbn,
                PageNumbers = book.PageNumbers,
                Sinopsis = book.Sinopsis,
                Title = book.Title,
                AuthorHasBookId = authorHasBooks.AuthorHasBookId,

            };

            return View(bookView);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BookView bookView)
        {
            if (id != bookView.Isbn)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var transaction = _context.Database.BeginTransaction())
                    {

                        try
                        {
                            _context.Update(bookView);
                            await _context.SaveChangesAsync();

                            var authorHasBook = new AuthorHasBook
                            {
                                AuthorId = bookView.AuthorId,
                                BookIsbn = bookView.Isbn,
                                AuthorHasBookId = bookView.AuthorHasBookId
                            };
                            _context.Update(authorHasBook);
                            await _context.SaveChangesAsync();
                            transaction.Commit();
                            TempData["message"] = "Registro editado correctamente";
                            return RedirectToAction(nameof(Index));
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            TempData["message"] = "Error:" + ex.Message;
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(bookView.Isbn))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "FullName", bookView.AuthorId);
            ViewData["EditorialId"] = new SelectList(_context.Editorials, "Id", "Campus", bookView.EditorialId);
            return View(bookView);
        }

        // GET: Books/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.Editorial)
                .FirstOrDefaultAsync(m => m.Isbn == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var book = await _context.Book.FindAsync(id);
            _context.Book.Remove(book);
            await _context.SaveChangesAsync();
            TempData["message"] = "Registro eliminado correctamente";
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(int id)
        {
            return _context.Book.Any(e => e.Isbn == id);
        }
    }
}
