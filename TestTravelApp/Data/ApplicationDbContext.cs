﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestTravelApp.Models;
using TestTravelApp.Models.Views;

namespace TestTravelApp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

       public DbSet<Author> Author { get; set; }
        public DbSet<Editorial> Editorials { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<AuthorHasBook> AuthorHasBook { get; set; }

    }
}
