# TestLibraryTravel

Para ejecutar el projecto 

1. Configura la conexion de base de datos en el archivo appsettings.json llave

"ConnectionStrings": {
    "DefaultConnection": "Server=DESKTOP-SIUOFN1;Database=TestTravelDB;User id=sa; password=123456;Trusted_Connection=false;MultipleActiveResultSets=true"
  },

2. Puede restaurar el backup de base de datos incluido en la ruta Data/DBBackup
	Archivo TestTravelDB_20210816221059.BAK
	
3. (Opcional si no restaura el backup)
		Abrir el package Manager console y ejecutar
		Update-Database
		Esto aplicara las migraciones y creara una base de datos desde cero
		
